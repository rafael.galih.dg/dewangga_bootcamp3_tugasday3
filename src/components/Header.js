import React , { useEffect, useState } from 'react'
import '../App.css';
import ListData from './ListData';
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import TextField from "@mui/material/TextField";
import { PersonAdd } from "@mui/icons-material";
import Fab from '@mui/material/Fab';

const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

export default function Header() {
    const [trigger, setTrigger] = useState(false);
    const [text, setText] = useState({
        name: "",
        address: "",
        hobby: ""
    });
    const [datas,setDatas] = useState([]);
  return (
    <div>
      
      <div className='header'>
        <h1 sx={{
          position: "absolute",
          top: "3%",
          left: "5%",
        }}>My App</h1>

        <Fab 
        sx={{
          position: "absolute",
          top: "3%",
          right: "3%",
        }}
        variant="extended" 
        color="primary"
        onClick={() => setTrigger(true)}>
          <PersonAdd sx={{ mr: 1 }} />
          Add User
        </Fab>
        <Modal
          open={trigger}
          onClose={() => setTrigger(false)}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Box sx={{ width: "100%" }}>
              <TextField
                fullWidth
                sx={{mt: 1, mb: 1}}
                label="Name"
                variant="outlined"
                onChange={(e) =>
                  setText({
                      ...text,
                      name: e.target.value,
                  })
              }
              />
              <TextField
                fullWidth
                sx={{mt: 1, mb: 1}}
                label="Address"
                variant="outlined"
                onChange={(e) =>
                  setText({
                    ...text,
                    address: e.target.value,
                  })
                }
              />
              <TextField
                fullWidth
                sx={{mt: 1, mb: 1}}
                label="Hobby"
                variant="outlined"
                onChange={(e) =>
                  setText({
                    ...text,
                    hobby: e.target.value,
                  })
                }
              />
            </Box>
            <Button
              variant="contained"
              color="success"
              onClick={() => {
                setText({
                    ...text,
                    id: datas.length,
                  })
                console.log(text)
                datas.push(text);
                console.log(datas);
                // console.log(datas);
              }}
              sx={{ mt: 5, ml:20}}
            >
              Save
            </Button>
          </Box>
        </Modal>
      </div>
      <div className='content'>
        <ListData sx={{width: "100vw"}} datas={datas} />
      </div>
    </div>
  )
}
