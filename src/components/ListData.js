import React, { useEffect, useState } from "react";

import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';
import ListItemText from '@mui/material/ListItemText';
import Typography from '@mui/material/Typography';
import { List } from "@mui/material";
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import { TextField } from '@mui/material';
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";
import Fab from '@mui/material/Fab';

const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

function Mui(props) {
    const [trigger, setTrigger] = useState(false);
    const [state,setState]= useState("");
    const [text, setText] = useState({
        id:0,
        name: "",
        address: "",
        hobby: ""
    });
    const datas = props.datas
    console.log(datas.length)
    if(datas.length == 0){
        return (
            <div>
            <h1>Data Tidak Ada</h1>
            </div>
        );
    }
    
    
    const result= datas.filter((x)=>x.name.toLowerCase().includes(state.toLowerCase()));
    

    return (
        <div>
            <Box>
                <Grid container>
                    <Grid xs={12} sx={{margin:5}}>
                        <TextField
                            label="Search"
                            onChange={(e)=>{
                                setState(e.target.value)
                            }}
                        />
                    </Grid>
                </Grid>
            </Box>
            <List sx={{ width: '100%', maxWidth: '100%', bgcolor: 'background.paper' }}>
                {state
                    ? result.map((data)=>{
                        return (
                            <div>
                                <ListItem alignItems="flex-start">
                                <ListItemText
                                    primary={data.name}
                                    secondary={
                                    <React.Fragment>
                                        <Typography
                                        sx={{ display: 'inline' }}
                                        component="span"
                                        variant="body2"
                                        color="text.primary"
                                        >
                                        {data.address}
                                        </Typography>
                                        <Typography 
                                        sx={{ 
                                            position: 'absolute',
                                            right: "10%",
                                            top: "30%"
                                        }}
                                        component="span"
                                        variant="body2"
                                        color="text.primary">
                                            {data.hobby}
                                        </Typography>
                                    </React.Fragment>
                                    }
                                />
                                </ListItem>
                                <Divider variant="inset" component="li" />
                            </div>
                        );
                    })
                    : datas.map((data,index) =>
                    <div>
                        <ListItem alignItems="flex-start">
                        <ListItemText
                            primary={data.name}
                            secondary={
                            <React.Fragment>
                                <Typography
                                sx={{ display: 'inline' }}
                                component="span"
                                variant="body2"
                                color="text.primary"
                                >
                                {data.address}
                                </Typography>

                                <Fab 
                                sx={{
                                position: "absolute",
                                top: "3%",
                                right: "3%",
                                }}
                                variant="extended" 
                                color="primary"
                                onClick={() => setTrigger(true)}>
                                Edit
                                </Fab>

                                <Modal
                                open={trigger}
                                onClose={() => setTrigger(false)}
                                aria-labelledby="modal-modal-title"
                                aria-describedby="modal-modal-description"
                                >
                                <Box sx={style}>
                                    <Box sx={{ width: "100%" }}>
                                    <TextField
                                        fullWidth
                                        sx={{mt: 1, mb: 1}}
                                        label="Name"
                                        variant="outlined"
                                        onChange={(e) =>
                                        setText({
                                            ...text,
                                            name: e.target.value,
                                        })
                                    }
                                    />
                                    <TextField
                                        fullWidth
                                        sx={{mt: 1, mb: 1}}
                                        label="Address"
                                        variant="outlined"
                                        onChange={(e) =>
                                        setText({
                                            ...text,
                                            address: e.target.value,
                                        })
                                        }
                                    />
                                    <TextField
                                        fullWidth
                                        sx={{mt: 1, mb: 1}}
                                        label="Hobby"
                                        variant="outlined"
                                        onChange={(e) =>
                                        setText({
                                            ...text,
                                            hobby: e.target.value,
                                        })
                                        }
                                    />
                                    </Box>
                                    <Button
                                    variant="contained"
                                    color="success"
                                    onClick={() => {
                                        datas[index].name = text.name;
                                        datas[index].address = text.address;
                                        datas[index].hobby = text.hobby;
                                    }}
                                    sx={{ mt: 5, ml:20}}
                                    >
                                    Save
                                    </Button>
                                </Box>
                                </Modal>
                                <Typography 
                                sx={{ 
                                    position: 'absolute',
                                    right: "10%",
                                    top: "30%"
                                }}
                                component="span"
                                variant="body2"
                                color="text.primary">
                                    {data.hobby}
                                </Typography>
                            </React.Fragment>
                            }
                        />
                        </ListItem>
                        <Divider variant="inset" component="li" />
                    </div>
                )
                }
            </List>
        </div>
    );
}

export default Mui;